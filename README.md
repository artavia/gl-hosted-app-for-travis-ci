# GL-Hosted App for Travis-CI

## Description
A starting point for a Travis-CI workflow for use with GitLab. 

## Contents of 'public' folder delivered to 'master' branch
What you need to know is that there is no deploy key in a gitlab configuration. In fact, you should assume that there is zero configuration that will take place with your .travis.yml file (unlike travis-ci for use with github applications). ~~The only potential pitfall was inadvertently setting up the project for "cache hell" where subsequent changes were never visible despite any previous efforts.~~ I had to go burrow into settings &raquo;&raquo; pages &raquo;&raquo; Remove pages in order to cull any previous build. I was unable, however, to get Travis-CI to initiate another similar build.

The trick ~~was~~ *would have been* to *first* build out a *.gitlab-ci.yml* file and make it work. Then, the next step ~~was~~ *would have been* to accomplish the same thing after removing the *.gitlab-ci.yml* file and performing the same task(s) with a new *.travis.yml* file instead. Minor glitches, notwithstanding, all in all this tiny experiment ~~was~~ *&lsquo;woulda&rsquo;&hellip; &lsquo;coulda&rsquo;&hellip; **should have been*** a success.

## Update
For the life of me, I **could not** get Travis-CI to cooperate with GitLab. Lord permitting, when I begin to experiment with Jenkins-CI within the next few days, I should find more success.

## Feel free to visit
Feel free to [visit the public prezzo](https://artavia.gitlab.io/gl-hosted-app-for-travis-ci/ "link to public prezzo") in order to get a taste for what ~~Travis-CI~~ *Gitlab-CI* can do to complement your GitLab projects.

## Baby steppin&rsquo; again!
This time I am building towards a bigger goal. Travis-CI is not the *be all end all* goal I aim to fulfill. Rather, it is &quot;an appetizer&quot; to get me ready for &quot;the main dish&quot; to come. I prefer to keep an air of mystery just to keep some people guessing (they know who they are).